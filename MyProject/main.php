<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/style.css">


  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <title>Коммунал для мамы ;-)</title>
</head>
<body>




<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="main.php">Коммуналка V1.0</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Счетчики <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Отчеты </a>
      </li>
    </ul>

  </div>
</nav>

<div class="container-fluid">
  <div class="row">
    <div class="col">
      <!-- Button trigger modal -->
      <a href="gaz.php" class="btn btn-info" style="width: 200px;">
         ГАЗ  <br />
        <i class="fas fa-burn fa-3x"></i>
      </a>
    </div>


    <div class="col">
      <!-- Button trigger modal -->
      <a href="#" class="btn btn-success" style="width: 200px;">
        ЭЛЕКТРИЧЕСТВО  <br />
        <i class="fas fa-bolt fa-3x"></i>
      </a>
    </div>


    <div class="col">
      <!-- Button trigger modal -->
      <a href="#" class="btn btn-danger"  style="width: 200px;">
        ВОДА  <br />
        <i class="fas fa-tint fa-3x"></i>
      </a>
    </div>


    <div class="col">
    <!-- Button trigger modal -->
    <a href="#" class="btn btn-secondary" style="width: 200px;">
      МУСОР  <br />
      <i class="fas fa-trash-alt fa-3x"></i>
    </a>
  </div>


    <div class="col">

      <a href="#" class="btn btn-dark" style="width: 200px;">
        ТЕЛЕФОН  <br />
        <i class="fas fa-phone fa-3x"></i>
      </a>

    </div>
    <div class="col">
    <!-- Button trigger modal -->
    <a href="#"  class="btn btn-primary" style="width: 200px; margin-right: 20px;">
    ИНТЕРНЕТ  <br />
    <i class="fab fa-internet-explorer fa-3x"></i>
  </a>
    <!-- Button trigger modal -->


</div>


  </div>
</div>
<div class="container-fluid table-main">
  <div class="row main-div">
    <h2 style="text-align: center; color: #2e5e86">2019 год</h2>
    <table class="table table-hover ">
      <thead>
      <tr class="table-danger">
        <th scope="col">#</th>
        <th scope="col">Статья</th>
        <th scope="col">Январь</th>
        <th scope="col">Февраль</th>
        <th scope="col">Март</th>
        <th scope="col">Апрель</th>
        <th scope="col">Май</th>
        <th scope="col">Июнь</th>
        <th scope="col">Июль</th>
        <th scope="col">Август</th>
        <th scope="col">Сентябрь</th>
        <th scope="col">Октябрь</th>
        <th scope="col">Ноябрь</th>
        <th scope="col">Декабрь</th>
      </tr>
      </thead>
      <tbody>
      <tr class="table-info">
        <th scope="row">1</th>
        <td>ГАЗ, грн.</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
      </tr>
      <tr class="table-info">
        <th scope="row">2</th>
        <td>ЭЛЕКТРИЧЕСТВО, грн.</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
      </tr>
      <tr class="table-info">
        <th scope="row">3</th>
        <td>ВОДА, грн</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
      </tr>
      <tr class="table-info">
        <th scope="row">4</th>
        <td>МУСОР, грн</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
      </tr>
      <tr class="table-info">
        <th scope="row">5</th>
        <td>ТЕЛЕФОН, грн.</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
      </tr>
      <tr class="table-info">
        <th scope="row">6</th>
        <td>ИНТЕРНЕТ, грн.</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
        <td>2 000.20</td>
        <td>1 230.47</td>
      </tr>
      <tr class="table-danger">
        <td colspan="row2" class="itog"></td>
        <td class="itog">ИТОГО, грн</td>
        <td class="itog">2 000.20</td>
        <td class="itog">1 230.47</td>
        <td class="itog">2 000.20</td>
        <td class="itog">1 230.47</td>
        <td class="itog">2 000.20</td>
        <td class="itog">1 230.47</td>
        <td class="itog">2 000.20</td>
        <td class="itog">1 230.47</td>
        <td class="itog">2 000.20</td>
        <td class="itog">1 230.47</td>
        <td class="itog">2 000.20</td>
        <td class="itog">1 230.47</td>
      </tr>

      </tbody>
    </table>
  </div>
</div>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>