/*
//Without loops

let userName = prompt('Enter you name','');
    if (userName === null || userName.length === 0){
        userName = prompt('Enter you name again! Pay attention!','');
    }
let userAge = Number(prompt('Enter you age',''));
if (userAge === null || userAge.length === 0 || isNaN(userAge)){
    userAge = prompt('Enter you age again! Pay attention! Write digital number','');
}
if (userAge<18){
    alert('You are not allowed to visit this website');

}else if (userAge>=18 && userAge<=22){

    if (confirm('Are you sure you want to continue?')){
        alert(`Welcome, ${userName}`);
    }
    else {
        alert('You are not allowed to visit this website');
    }
}
else {
    alert(`Welcome, ${userName}`)
}*/


// with loops for Artem ;-)

let userName = prompt('Enter you name','');
    while (userName == null || userName.length === 0){
    userName = prompt('Enter you name again! Pay attention!','');
}
let userAge = Number(prompt('Enter you age',''));
while (userAge == null || userAge == "" || isNaN(userAge)){
    userAge = prompt('Enter you age again! Pay attention! Write digital number','');
}

if (userAge<18){
    alert('You are not allowed to visit this website');

}else if (userAge>=18 && userAge<=22){

    if (confirm('Are you sure you want to continue?')){
        alert(`Welcome, ${userName}`);
    }
    else {
        alert('You are not allowed to visit this website');
    }
}
else {
    alert(`Welcome, ${userName}`)
}